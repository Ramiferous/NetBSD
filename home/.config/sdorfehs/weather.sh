#!/bin/sh

set -e

if [ -f ~/.config/sdorfehs/wearther.txt ]; then
    rm ~/.config/sdorfehs/weather.txt
fi

curl -s wttr.in/-37.731665,144.995026?format='%C+%t\n' | sed 30q > ~/.config/sdorfehs/weather.txt
