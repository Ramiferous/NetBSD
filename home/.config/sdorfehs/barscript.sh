#!/bin/sh

set -eu
while :
do

	# Packages
	PKGS=$(pkg_info | wc -l)
	PKGS=${PKGS##* }		# strip leading spaces

	# Package Updates
	#PKGUP=$(cat $HOME/.config/sdorfehs/updates.txt)
    PKGUP=$(pkgin -l '<' list | wc -l)
	PKGUP=${PKGUP##* }      # strip leading spaces

    # Workspace
    AWS="$(xprop -root '\t$0' _NET_CURRENT_DESKTOP | cut -f 2)"
    WS="$(($AWS + 1))"      # because workspaces begin at '0'
    NWS="$(xprop -root '\t$0' _NET_NUMBER_OF_DESKTOPS | cut -f 2)"

	# Volume
	VOL=$(mixerctl -n outputs.master2)
	VOL=${VOL%,*}			# strip other channel
	VOL=$(( (VOL * 100) / 254 ))
	MUTE=$(mixerctl -n outputs.master5.mute)
	if [ $MUTE = on ]
	then	MUTE="^fg(red) ^fg() "
	else	MUTE="  "
	fi

	# Date and time
    D=$(date '+%a %d %b')
    T=$(date '+%I:%M%p')

	# Weather
	WTTR=$(cat $HOME/.config/sdorfehs/weather.txt)

	# Print
    echo "[ $WTTR ]  [ $MUTE$VOL% ]  [   $PKGS    $PKGUP ]  [   $D    $T ]  [ 󰇄  $WS ]" > ~/.config/sdorfehs/bar
	sleep 1

done
