#!/bin/sh

set -e

# Define paths
UPDATES_FILE="$HOME/.config/sdorfehs/updates.txt"
BAR_SCRIPT="$HOME/.config/sdorfehs/barscript.sh"

# Check for updates & pipe to status bar
if [ -f "$UPDATES_FILE" ]; then
    rm "$UPDATES_FILE"
fi
doas pkgin update
pkgin -l '<' list | wc -l > "$UPDATES_FILE"

# Re-run bar output script
sh $BAR_SCRIPT &
