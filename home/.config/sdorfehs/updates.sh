#!/bin/sh

set -e

if [ -f ~/.config/sdorfehs/updates.txt ]; then
    rm ~/.config/sdorfehs/updates.txt
fi

pkg_chk -u -q | wc -l > ~/.config/sdorfehs/updates.txt

