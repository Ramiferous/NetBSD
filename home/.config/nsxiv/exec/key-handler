#!/bin/sh
#
# Dependencies: dmenu, feh, notify-send, image-magick, gimp, mediainfo, xclip

DMENU=${DMENU:-dmenu}
NOTIFY="notify-send -a" # set timeout with '-t' or in LXQt settings

while read file
do
        case "$1" in

    # Set image as wallpaper
    "w")
        feh --bg-scale --no-fehbg "$file" && $NOTIFY "$file" "Set as Wallpaper" &
        ;;

    # Copy file to another location
    "c")
		[ -z "$destdir" ] &&
            destdir="$(sed 's/s.*#.*$//;/^s*$/d' ${XDG_CONFIG_HOME:-$HOME/.config}/shell/bm-dirs |
            awk '{print $2}' |
            $DMENU -p "Copy file(s) to where?" |
            sed "s|~|$HOME|g")"
		[ ! -d "$destdir" ] && $NOTIFY "$destdir" "Not a directory, cancelled" && exit
		cp "$file" "$destdir" && $NOTIFY "Copied To" "$destdir" &
		;;

	# Move file to another location
    "m")
		[ -z "$destdir" ] &&
            destdir="$(sed -Eg 's/\s+#.*$//; s/^\s*#//; /^\s*$/d' ${XDG_CONFIG_HOME:-$HOME/.config}/shell/bm-dirs |
            awk '{print $2}' |
            $DMENU -p "Move file(s) to where?" |
            sed "s|~|$HOME|g")"
		[ ! -d "$destdir" ] && $NOTIFY "$destdir" "Not a directory, cancelled" && exit
		mv "$file" "$destdir" && $NOTIFY "Moved To" "$destdir" &
		;;

	# Rotate/flop image
	"r")
		convert -rotate 90 "$file" "$file" ;;
	"R")
		convert -rotate -90 "$file" "$file" ;;
	"f")
		convert -flop "$file" "$file" ;;

	# Copy file location
	"y")
		echo -n "$file" | tr -d '\n' | xclip -sel clip &&
            $NOTIFY "File Location" "$(xclip -o -sel clip)"
		;;

    # Delete selected images, confirm using dmenu
    "d")
		[ "$(printf "No\\nYes" |
            $DMENU -p "Really delete $file?")" = "Yes" ] &&
            rm "$file" && $NOTIFY "File Deleted" "Bye Bye"
        ;;

    # Open image in gimp
	"g")
        if ! command -v gimp 2>&1 >/dev/null
        then
            $NOTIFY "Gimp" "Command not found"
            exit 1
        else
            gimp "$file"
        fi
        ;;

	# Print mediainfo to notification
	"i")
        notify-send -t 15000 -a "Media Info" "$(mediainfo "$file")" ;;

	# Upload image & copy url to clipboard
    "u")
        pb 0x0 "$file" |
		xclip -sel clip && $NOTIFY "Image Uploaded" "$(xclip -o -sel clip)"
		;;

		esac
done
