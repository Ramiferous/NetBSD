#!/bin/sh

color=$(xcolor)

if [ -n "$color" ]; then
        #temp=$(mktemp -t --suffix ".png")
        temp=$(mktemp /tmp/xcolorpicker.png)
        convert -size 100x100 xc:$color $temp
        echo $color | xsel -ib
        #notify-send -i $temp "Colorpicker" "$color"
        printf "IMG:$temp\tColorpicker\t$color\n" >$XNOTIFY_FIFO
        rm /tmp/xcolorpicker.png
fi
