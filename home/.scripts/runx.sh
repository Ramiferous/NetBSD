#!/bin/sh

set -eu -o pipefail

die() { echo >&2 "$0: $*"; exit 1; }

test -e /tmp/.X0-lock && die "X server already running"

export XAUTHORITY=$HOME/.Xauthority	# judge dredd
SA=$(mktemp $HOME/.serverauth.XXXXXX)	# more XXXs => more hardcore

CK=$(/usr/bin/openssl rand -hex 16)	# make cookie
test -z "$CK" && die "couldn't make cookie"

trap "rm -f '$SA' '$XAUTHORITY'" EXIT HUP INT QUIT ILL TRAP BUS TERM
trap "" TSTP				# no shell with Ctrl-Z

echo "add :0 . $CK" | xauth -q -f $SA	# guts
unset CK				# leave no cookie behind

cp -fp $SA $XAUTHORITY			# assume 1 server 1 session

# off to the races
/usr/local/bin/runsafe $(xinit $HOME/.xinitrc -- /usr/X11R7/bin/X :0 -auth $SA)
