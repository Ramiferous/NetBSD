#!/bin/sh

OSABI=$HOME/.pkgsrc/pkgtools/osabi
XLINKS=$HOME/.pkgsrc/pkgtools/x11-links
WIP=$HOME/.pkgsrc/wip

su root -c "

	# remove orphan packages and clean pkgin
	pkgin -y autoremove && pkgin clean

	# install osabi
	cd $OSABI
	su $USER -c 'make -s'
	make install clean distclean clean-depends

	# install x11-links
	cd $XLINKS
	su $USER -c 'make -s'
	make install clean distclean clean-depends

	# update pkgsrc/wip
	cd $WIP
	su $USER -c 'git pull -r'

"
