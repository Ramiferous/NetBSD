#!/bin/sh

set -eu -o pipefail

HOUSE="https://codeberg.org/pfr/netbsd"
SHFM="https://github.com/pfr-dev/shfm"
FMUI="https://github.com/pfr-dev/fmui"
UEBERZUG="https://github.com/pfr-dev/ueberzug" # build dir py-Ueberzug


mkdir -p \
    $HOME/.config \
    $HOME/.git/codeberg \
    $HOME/.git/github \
    $HOME/.media \
    $HOME/.mpd \
    $HOME/.password-store \
    $HOME/.scripts \
    $HOME/.ssh \
    $HOME/.vim \
    $HOME/.weechat

git clone $HOUSE $HOME/.git/codeberg/
git clone $SHFM $HOME/.git/github/
git clone $FMUI $HOME/.git/github/
git clone $UEBERZUG $HOME/.git/github/

# move in the furniture
cp -r $HOME/.git/codeberg/netbsd/home/* $HOME

# suas not available yet...
su root -c "cp -r $HOME/.git/codeberg/netbsd/usr/local/bin/* /usr/local/bin"

# now use suas to move the rest
suas cp -r $HOME/.git/codeberg/netbsd/usr/pkg/etc/* /usr/pkg/etc
suas cp -r $HOME/.git/codeberg/netbsd/etc/X11/* /etc/X11/
suas cp -r $HOME/.git/codeberg/netbsd/etc/powerd/* /etc/powerd/
suas cp $HOME/.git/codeberg/netbsd/boot.cfg /

# install some packages
suas pkgin install $(cat $HOME/pkglist.txt)

echo "\

    ...now reboot and hope for the best"
