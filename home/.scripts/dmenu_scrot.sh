#!/usr/bin/env bash

set -o pipefail

DMENU=${DMENU:-dmenu}
IMG_PATH=$HOME/Pictures/dumps/
UL="pb 0x0"     # pb [0x0/envs/catbox]
TIME=5000       # seconds notification should remain visible
FILENAME=$(date +%Y-%m-%d_@_%H-%M-%S-scrot.png)

prog="1.Fullscreen
2.Delay_Fullscreen
3.Draw_Section
4.Window
5.Fullscreen_UL
6.Delay_Fullscreen_UL
7.Draw_Section_UL
8.Delay_Draw_UL
9.Window_UL"

# dmenu vars set in ~/.profile and call ${DMENU}
cmd=$($DMENU -l 20 -p 'Choose Screenshot Type' <<< "$prog")

cd $IMG_PATH
case ${cmd%% *} in

    1.Fullscreen)           scrot -d 1 $FILENAME && notify-send -t $TIME -a "Screenshot" "$IMG_PATH$FILENAME" ;;
	2.Delay_Fullscreen)     scrot -d 4 $FILENAME && notify-send -t $TIME -a "Screenshot" "$IMG_PATH$FILENAME" ;;
	3.Draw_Section)	        scrot -s $FILENAME && notify-send -t $TIME -a "Screenshot: Section" "$IMG_PATH$FILENAME" ;;
    4.Window)               scrot -ub -d 1 $FILENAME && notify-send -t $TIME -a "Screenshot: Window" "$IMG_PATH$FILENAME" ;;
    5.Fullscreen_UL)        scrot -d 1 $FILENAME && $UL $FILENAME | xclip -sel clip && notify-send -t $TIME -a "Screenshot Uploaded" "$(xclip -o -sel clip)" && rm $IMG_PATH$FILENAME ;;
    6.Delay_Fullscreen_UL)  scrot -d 4 $FILENAME && $UL $FILENAME | xclip -sel clip && notify-send -t $TIME -a "Sreenshot Uploaded" "$(xclip -o -sel clip)" && rm $IMG_PATH$FILENAME ;;
    7.Draw_Section_UL)      scrot -s $FILENAME && $UL $FILENAME | xclip -sel clip && notify-send -t $TIME -a "Screenshot Uploaded" "$(xclip -o -sel clip)" && rm $IMG_PATH$FILENAME ;;
    8.Delay_Draw_UL)        scrot -s -d 10 $FILENAME && $UL $FILENAME | xclip -sel clip && notify-send -t $TIME -a "Screenshot Uploaded" "$(xclip -o -sel clip)" && rm $IMG_PATH$FILENAME ;;
    9.Window_UL)            scrot -ub -d 1 $FILENAME && $UL $FILENAME | xclip -sel clip && notify-send -t $TIME -a "Screenshot: Window" "$IMG_PATH$FILENAME  !Uploaded: $(xclip -o -sel clip)" && rm $IMG_PATH$FILENAME ;;

  	*)		exec "${cmd}" ;;
esac
