#!/bin/sh

set -e PIPEFAIL

curl -fsSL "https://api.github.com/repos/jonz94/Sarasa-Gothic-Nerd-Fonts/releases/latest" | \
    grep "browser_download_url.*sarasa-term-cl-nerd-font.zip" | \
        head -n 1 | \
    cut -d '"' -f 4 | \
        xargs curl -fL -o "sarasa-term-cl-nerd-font.zip"
