#!/bin/sh
#
# Simple script to combine i3lock with i3lockr

set -e

# Get blur using only i3lock, scrot and imagemagick
#scrot /tmp/screen_locked.png
#mogrify -noise 0x3 -blur 0x5 /tmp/screen_locked.png
#i3lock -i /tmp/screen_locked.png
#rm /tmp/screen_locked.png

# Blur and cool icons the easy way
# Install i3lock & i3lockr
#i3lockr -i $HOME/Pictures/logos/NetBSD-tbr-small.png --blur 15 -- -e -u
i3lockr --blur 25 -- -e -u
