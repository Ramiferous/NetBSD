#!/bin/sh
#
# requires the netpbm package to be installed

set -o pipefail

# set your preferred screenshot directory
DIR=$HOME/Pictures/dumps

# output file format
FILE="$(date +"root.%d-%m-%Y.%H:%M").png"

# wait, if you want..
sleep 5;

# shoot your shot
xwd -silent -root | xwdtopnm | pnmtopng > $DIR/$FILE

# send notifiation
notify-send -t 5000 -a "Screenshot Saved" "~/Pictures/dumps"
