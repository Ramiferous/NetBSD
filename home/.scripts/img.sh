#!/usr/bin/env bash
#
# script to display mages using ueberzug
# place this script in $PATH, call it img
# to run do: img ~/path/to/image.png 0 0 30 30

[ -z "$5" ] && echo "Usage: $0 <image> <x> <y> <max height> <max width>" && exit
source "`ueberzug library`"

ImageLayer 0< <(
    ImageLayer::add [identifier]="example0" [x]="$2" [y]="$3" [max_width]="$5" [max_height]="$4" [path]="$1"
     read
)
