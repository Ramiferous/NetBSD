#!/bin/sh
#
# sued: sudoedit (most of) as a shell-script
#
# Test:
# sudo install -d -m 755 $'hello\n"world\n;>foo\t'\'$'$bar\n'
# sudo cp /etc/passwd $'hello\n"world\n;>foo\t'\'$'$bar\n'/.
# As root:
# echo x > $'hello\n"world\n;>foo\t'\'$'$bar\n'/$'xxx\n  yyy\t\n'
# sued.sh hell*/xxx* hell*/passwd

umask 077                # be paranoid
ME=${0##*/}
MAGIC=SUED_FiFi_the_FLeA_CHILD

# Bourne-shell string quoting
# turns:
#    hello world   -> 'hello world'
#    hello 'world' -> 'hello '\''world'\''
#

# q()
# {
#     printf "'"
#     printf '%s' "$1" | sed -Ee "s/'/'\\\\''/g"
#     printf "'"
# }

q()
{
    awk "
BEGIN {
    printf(\"'\")
    if (ARGC != 2)
        exit 0
    s=ARGV[1]
    n=length(s)
    for (i=1; i <= n; i++) {
        c=substr(s, i, 1)
        if (c == \"'\")
            printf(\"'\\\\%c'\", c)
        else
            printf(\"%c\", c)
    }
    printf(\"'\")
}" "$1"
}

# Check for invalid things:
#   empty ("") filenames
#   files in writable dirs.
#
isok()
{
    if [ -z "$1" ]
    then    echo "$ME: skipping empty filename" >&2
        return 1
    fi
    if [ -w "$(/usr/bin/dirname "$1")" ]
    then    echo "$ME: $1: editing files in a writable directory is not permitted" >&2
        return 1
    fi
    return 0
}

# Shell quoting is a pain in the southern-hemispheres and we don't
# want to use a helper script, so just re-exec ourself and do the
# real work in the re-exec'd (priviledged) instance.
#
if [ "$1" != $MAGIC ]            # Primary instance
then    set -eu
    E=$(q "${EDITOR:=vi}")
    U=$(/usr/bin/id -u)
    G=$(/usr/bin/id -g)
    F=""
    rc=0
    N=0
    for A
    do    if isok "$A"
        then    F="$F $(q "$A")"
            N=$((N + 1))
        else    rc=1
        fi
    done
    if [ $N -eq 0 ]
    then    if [ $rc -eq 1 ]    # error msg. already printed
        then    exit 1
        fi
        echo "Usage: $ME FILE..." >&2
    fi
    exec su root -c "$0 $MAGIC $E $U $G $F"
else    set -eu -o pipefail        # Re-exec'd instance
    E=$(q "$2")            # snag user's editor
    U=$3                #  " orig UID
    G=$4                #  " orig GID
    shift 4

    # Create temporary files
    #
    TFF=""
    N=0
    for f
    do    if [ \! -f "$f" ]
        then    echo "$ME: $f: non-existent or non-regular file" >&2
            continue
        fi
        FU=$(/usr/bin/stat -f %u "$f")
        FG=$(/usr/bin/stat -f %g "$f")
        FM=$(/usr/bin/stat -f %Mp%Lp "$f")
        T=$(/usr/bin/basename "$f")
        T=$(/usr/bin/mktemp "/var/tmp/$T.XXXXXX")
        /usr/sbin/chown $U:$G "$T"
        /bin/cat "$f" > "$T"    # copy contents into temp. file
        eval U$N=$FU        # an array! an array! my kingdom
        eval G$N=$FG        #   for an array!
        eval M$N=$FM
        f=$(q "$f")        # quote for eval
        eval FN$N="$f"        # stash orig. name
        T=$(q "$T")        # quote for eval
        eval TF$N="$T"        # stash temp. name
        TFF="$TFF $T"        # collect
        N=$((N + 1))
    done

    # Invoke editor as (orig. user) on temp. files
    #
    if [ $N -gt 0 ]
    then    su -l "$(id -nu $U)" -c "$E $TFF"
    else    exit 1
    fi
    
    # Copy temp. files back over originals
    #
    I=0
    while [ $I -lt $N ]
    do    eval T=\$TF$I                # extract temp. file name
        eval F=\$FN$I                #    "    orig.    "
        if /usr/bin/cmp -s "$T" "$F"
        then    echo "$ME: $F: unchanged"
            /bin/rm -f "$T"            # remove temp. file
        else    if [ $(/usr/bin/stat -f %z "$T") -eq 0 ]
            then    printf '%s: not replacing "%s" with 0-byte "%s"\n' \
                    "$ME" "$F" "$T" >&2
                /bin/rm -f "$T"
            else    eval U=\$U$I        # set orig. file bits
                eval G=\$G$I
                eval M=\$M$I
                /usr/sbin/chown $U:$G "$T"
                /bin/chmod $M "$T"    # chmod _after_ chown
                /bin/mv "$T" "$F"
            fi
        fi
        I=$((I + 1))
    done
fi