#!/bin/sh -e
#
# generate a wallpaper with some static
# deps: imagemagick, color
#

[ "$1" ] || set -- "$(xcolor)"

f="/tmp/noisewall.png"

convert -size 128x128 xc:"$*" \
    -separate -seed 1000 \
    -attenuate 0.13 \
    +noise gaussian \
    -combine -colorspace sRGB "$f"

feh --bg-tile "$f"
