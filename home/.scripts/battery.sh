#!/bin/sh
BAT_PERC="$(envstat -s acpibat0:charge | tail -1 | sed -e 's,.*(\([ ]*[0-9]*\)\..*,\1,g')%"

BAT_STATE="$(envstat -d acpibat0 | awk 'FNR == 10 {print $2}')"

if [ "${BAT_STATE}" = "TRUE" ]; then
    STATE='Charging'
else
    STATE='Discharging'
fi

echo $STATE $BAT_PERC
