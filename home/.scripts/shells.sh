#!/bin/sh

set -eu -o pipefail

clear

echo
read -p "Choose your shell:

1) tty.sdf.org
2) beastie.sdf.org
3) tilde.pink
4) g.nixers.net

> " action

if [ "$action" = "1" ]; then
    ssh pfr@tty.sdf.org
fi

if [ "$action" = "2" ]; then
    ssh pfr@beastie.sdf.org
fi

if [ "$action" = "3" ]; then
    ssh pfr@tilde.pink
fi

if [ "$action" = "4" ]; then
    ssh pfr@g.nixers.net
fi
