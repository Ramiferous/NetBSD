!/bin/sh
#
#  Generate a wallpaper with some static
#

[ "$1" ] || set -- "$(xcolor)"

fi="/tmp/tile.png"

convert -size 128x128 canvas:"$1" \
        -separate -seed 1000 \
	    -attenuate 0.13 \
        +noise gaussian \
        -combine -colorspace sRGB "$fi"

feh --bg-tile "$fi"
