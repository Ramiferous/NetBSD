#!/bin/sh

set -e

if [ -f /home/dave/.config/sdorfehs/updates.txt ]; then
    rm /home/dave/.config/sdorfehs/updates.txt
fi

echo "Checking for available updates..."
pkg_chk -u -q | wc -l > /home/dave/.config/sdorfehs/updates.txt

