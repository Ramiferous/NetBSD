#
# Don't execute this file; source it.
#

# Bourne-shell string quoting
# turns:
#	hello world     -> 'hello world'
#	hello 'world'   -> 'hello '\'world\'
#	\hello "$world" -> '\hello "$world"'

q() {
	awk -f - -- "$@" <<\EoF
BEGIN {
	PAT = "^[-_./[:alnum:]]+$"
	for (i = 1; i < ARGC; i++) {
		s = ARGV[i]
		while (n = index(s, "'")) {
			ss = substr(s, 1, n-1)
			if (match(ss, PAT))
				S = S ss
			else if (length(ss))
				S = S "'" ss "'"
			S = S "\\'"
			s = substr(s, n+1)
		}
		if (match(s, PAT))
			S = S s
		else if (length(s) || !length(ARGV[i]))
			S = S "'" s "'"
		S = S ((i < ARGC-1) ? " " : "")
	}
	printf "%s", S
}
EoF
}
