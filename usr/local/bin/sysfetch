#!/usr/bin/env bash

# color escapes
BLK="\e[30m"
RED="\e[31m"
GRN="\e[32m"
YLW="\e[33m"
BLU="\e[34m"
PUR="\e[35m"
CYN="\e[36m"
WHT="\e[37m"
GRY="\e[90;1m"
RST="\e[0m"
BLD="\033[1m"

# bars
#FULL=▓
#EMPTY=░
FULL=━
EMPTY=─
#FULL=─
#EMPTY=┄

name=$USER
host=$(uname -n)
battery=$(envstat -s acpibat0:charge | tail -1 | sed -e 's,.*(\([ ]*[0-9]*\)\..*,\1,g')
distro=$(uname -sm)
kernel=$(uname -r)
pkgs=$(pkg_info | wc -l | sed -e 's/^[ \t]*//')
colors='alduin'
font=$(grep family /home/dave/.config/alacritty/alacritty.toml | awk '{print $3}' | uniq | tr -d '"')
wm=$(tail -n 1 "${HOME}/.xinitrc" | cut -d ' ' -f 2)
shell=$(basename $(printenv SHELL))
term=$(echo $TERM)
uptm=$(uptime | awk -F, '{sub(".*up ",x,$1);print $1}' | sed -e 's/^[ \t]*//;s/:/ hr /')
machine="$(sysctl -n machdep.dmi.system-version)"

#Cleanup first
clear

# find the center of the screen
COL=$(tput cols)
ROW=$(tput lines)
((PADY = ROW / 2 - 1 - 22 / 2))
((PADX = COL / 2 - 32 / 2))

for ((i = 0; i < PADX; ++i)); do
	PADC="$PADC "
done

for ((i = 0; i < PADY; ++i)); do
	PADR="$PADR\n"
done

# vertical padding
printf "%b" "$PADR"
printf "\n"

# Print Tree
BAR="████"
OUTT="$BLK$BAR$RED$BAR$GRN$BAR$YLW$BAR$BLU$BAR$PUR$BAR$CYN$BAR$WHT$BAR$RST"
printf "%s%b" "$PADC" "$OUTT"
printf "\n\n"

# greetings
printf "%s%b" "$PADC" "           hello $RED$BLD$name$RST\n"
printf "%s%b" "$PADC" "       welcome to $GRN$BLD$host$RST\n"
printf "%s%b" "$PADC" "   i've been awake for $CYN$BLD$uptm$RST\n\n"

# environment
#printf "%s%b" "$PADC" "$YLW       machine $BLU⏹ $RST$machine\n"
printf "%s%b" "$PADC" "$YLW        distro $BLU⏹ $RST$distro\n"
printf "%s%b" "$PADC" "$YLW        kernel $BLU⏹ $RST$kernel\n"
printf "%s%b" "$PADC" "$YLW      packages $BLU⏹ $RST$pkgs\n"
printf "%s%b" "$PADC" "$YLW            wm $BLU⏹ $RST$wm\n"
printf "%s%b" "$PADC" "$YLW          term $BLU⏹ $RST$term\n"
printf "%s%b" "$PADC" "$YLW         shell $BLU⏹ $RST$shell\n"
printf "%s%b" "$PADC" "$YLW          font $BLU⏹ $RST$font\n"
printf " $RST\n"

# progress bar
draw() {
	perc=$1
	size=$2
	inc=$((perc * size / 100))
	out=
	color="$3"
	for v in $(seq 0 $((size - 1))); do
		test "$v" -le "$inc" &&
			out="${out}\e[1;${color}m${FULL}" ||
			out="${out}\e[0;37m${EMPTY}"
	done
	printf $out
}

# cpu
cpu=$(grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}')
c_lvl=$(printf "%.0f" $cpu)
printf "%b" "$RST$PADC"
printf "   $PUR%-4s $WHT%-5s %-25s \n" " cpu" "$c_lvl%" $(draw $c_lvl 15 35)

# ram
ram=$(free | awk '/Mem:/ {print int($3/$2 * 100.0)}')
printf "%b" "$RST$PADC"
printf "   $PUR%-4s $WHT%-5s %-25s \n" " ram" "$ram%" $(draw $ram 15 35)

# battery
#battery=$(envstat -s acpibat0:charge | tail -1 | sed -e 's,.*(\([ ]*[0-9]*\)\..*,\1,g')
#printf "%b" "$RST$PADC"
#printf "   $PUR%-4s $WHT%-5s %-25s \n" " bat" "$battery%" $(draw $battery 15 35)

# temperature
temp=$(envstat | awk '/cpu0/ {print $3}' | sed 's/.000//')
printf "%b" "$RST$PADC"
printf "   $PUR%-4s $WHT%-5s %-25s \n" " tmp" "$temp°c " $(draw $temp 15 35)

# hide the cursor and wait for user input
tput civis
read -n 1

# give the cursor back
tput cnorm
